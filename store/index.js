import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        strict: false,
        state: {
            loanding: false,
            data: {
                logged: false,
                success: false,
                username: '',
                last_access: '',
                brand_image: '',
                acl: '',
                token: ''
            }
        },
        mutations: {
            logout(state) {
                $cookies.remove('pbx')
                state.data = {}
            },
            authenticate: function (state, payload) {
                state.data = payload
                $cookies.set('pbx', payload, {
                    path: '/',
                    maxAge: 60 * 60 * 24 * 7
                })
            },
            onLoading(state, payload) {
                state.loanding = (payload) ? true : false
            }
        },
        getters: {
            isAuthenticated: function (state) {
                return (state.data.logged) ? true : false
            },
            getToken(state) {
                return state.data.token;
            },
            getUserName(state) {
                return state.data.full_name;
            },
            getLastAccess(state) {
                return state.data.last_access;
            },
            getACL(state) {
                if(state.data.acl === 'A'){
                    return 'A';
                }else if(state.data.acl === 'F'){
                    return 'F';
                }else if(state.data.acl === 'C'){
                    return 'C';
                }
            },
            getBrandImage(state) {
                return (state.data.brand_image) ? state.data.brand_image : ''
            }
        }
    })
}

export default createStore