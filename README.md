# Allure

git clone git@bitbucket.org:rogergl44/front-end.git

By: Rogério Gomes

Front - End Allure

Acesso a api: [http://allure-api.wcast.com.br](http://allure-api.wcast.com.br).

## Installation

Execute: 

```bash
npm install```

Para rodar o servidor:

```bash
npm run dev``
