module.exports = {
  /*
    ** Headers of the page
    */
  head: {
    title: 'Allure',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Allure' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/img/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' },
      { rel: 'stylesheet', href: 'https://unpkg.com/vue-css-donut-chart/dist/vcdonut.css' },
      { rel: 'stylesheet', href: '/css/loading.css' },
      {
        rel: 'apple-touch-icon',
        href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/img/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/img/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/img/favicon-16x16.png' },
      { rel: 'manifest', href: '/img/site.webmanifest' },
      { rel: 'mask-icon', href: '/img/safari-pinned-tab.svg', color: '#5bbad5' },
      { name: 'msapplication-TileColor', content: '#ffc40d' },
      { name: 'theme-color', content: '#ffffff' }
    ],
    script: [
      { src: 'https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js' },
      { src: 'https://cdn.polyfill.io/v2/polyfill.min.js' }
    ]
  },
  plugins: [
    '~/plugins/axios',
    '~/plugins/vuetify.js',
    '~/plugins/vue-cookies.js',
    '~/plugins/chart.js'
  ],
  loading: { color: '#3dff25' },
  modules: ['@nuxtjs/axios', 'cookie-universal-nuxt'],
  axios: {
    baseURL: 'http://allure-api.wcast.com.br'
  },
  css: ['~/assets/style/app.styl'],
  build: {
    extractCSS: true,
    extend (config, ctx) {
    }
  }
}
