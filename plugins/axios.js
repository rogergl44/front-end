export default function ({$axios}) {
    $axios.onRequest(config => {
        if(config.headers.notoken === 1){
            config.headers.Authorization = ''
        }else{
            config.headers.Authorization = window.localStorage.getItem('pbx')
        }
    })

    $axios.onResponse(response => {
        console.log(response.data.token)
    })

    $axios.onError(error => {
        const code = parseInt(error.response && error.response.status)
        if (code === 400) {
            alert(code)
        }
    })
}